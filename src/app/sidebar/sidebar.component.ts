import { Component, Input, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  // @Input('check') public check?:any

  // constructor() {}
  constructor(public appService: AppService) {
  }
  ngOnInit() {

  }

}

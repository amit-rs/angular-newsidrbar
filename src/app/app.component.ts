import { identifierName } from '@angular/compiler';
import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularSidebar';
  isClicked:boolean=false;
  constructor(public appService: AppService) {
    
  }
  ngOnInit() {}
  
  labelClickHandler() {
    this.isClicked=!this.isClicked;
    this.appService.clickLable(this.isClicked);
  }
}

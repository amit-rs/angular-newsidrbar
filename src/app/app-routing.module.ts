import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { CollectionsComponent } from './collections/collections.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './logout/logout.component';
import { MessagesComponent } from './messages/messages.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path:'dashboard',component:DashboardComponent},
  {path:'analytics',component:AnalyticsComponent},
  {path:'messages',component:MessagesComponent},
  {path:'collection',component:CollectionsComponent},
  {path:'user',component:UsersComponent},
  {path:'setting',component:SettingsComponent},
  {path:'notification',component:NotificationsComponent},
  {path:'account',component:AccountComponent},
  {path:'logout',component:LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [DashboardComponent, AnalyticsComponent, MessagesComponent, CollectionsComponent,
  UsersComponent, SettingsComponent, NotificationsComponent, AccountComponent, LogoutComponent]
